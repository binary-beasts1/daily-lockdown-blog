-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 07:38 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lockdown_blog`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `selectID` ()  READS SQL DATA
SELECT postID FROM post$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SelectPostID` ()  READS SQL DATA
SELECT postID FROM post$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `commentID` int(11) NOT NULL,
  `postID` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `guestEmail` varchar(300) NOT NULL,
  `guestName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`commentID`, `postID`, `content`, `guestEmail`, `guestName`) VALUES
(41, 3, 'facewine comment', 'juan:@mail.com', 'juan'),
(42, 3, 'New comment here! Hi!', 'toby@hotmail.com', 'Toby'),
(43, 23, 'gadsg', 'sagsd', 'gadsd'),
(45, 36, 'This looks like a lot of fun!', 'anna@gmail.com', 'Anna'),
(46, 1, 'Another excuse for easter chocolate - not complaining!', 'tim@tim.com', 'Tim'),
(47, 24, 'Wow what a great idea!', 'greatgreta@me.com', 'Greta'),
(48, 26, 'What a great idea! Time to get the brushes out I say!', 'karen@baren.com', 'Karen'),
(49, 26, 'This is the best way to use up your time and do something productive! Does anybody have any idea where I can buy some filler?', 'johnbohn@hotmail.com', 'John'),
(52, 26, 'Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. Let&#39;s make this comment really long. ', 'longcomment@hotmail.com', 'Long');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `postID` int(10) NOT NULL,
  `author` varchar(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `published` date NOT NULL,
  `image1` varchar(1000) NOT NULL,
  `image2` varchar(1000) NOT NULL,
  `postcontent` longtext NOT NULL,
  `feature` enum('YES','NO') NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`postID`, `author`, `title`, `published`, `image1`, `image2`, `postcontent`, `feature`) VALUES
(1, 'Chloe', 'Easter Trail', '2020-04-08', 'public/images/posts/EasterTrail.jpeg', 'public/images/posts/EasterTrail2.jpeg', 'Looking for something fun to do with the kids? Even if you don&#39;t have outdoor space, you can still spend an afternoon on an easter egg hunt. Older children can plan a route around the house for their siblings, leaving clues in certain locations to lead them to a prize. For younger children, hide prizes around a few rooms of the house. Make the challenge as complicated as you please - children will work hard when chocolate is on offer! No chocolate? Bake cupcakes! Hide something small from that dusty box of little gifts you keep for birthdays you&#39;d forgotten about? Good luck! ', 'NO'),
(2, 'Chloe', 'Remote Murder Mystery', '2020-04-09', 'public/images/posts/RemoteMurderMystery.jpeg', 'public/images/posts/RemoteMurderMystery2.jpeg', 'Want to test your key-worker talents? Here&#39;s a game to entertain you for a whole evening with some buddies whilst learning what it&#39;s like to spend enough time outside your home to commit a murder. Imagine! All it requires is a few friends, a group video call platform and our template below! In the post-pandemic world of 2025, you aim to arrest the crazy culprit responsible for killing the owner of Vid Vaccines. In a dramatic story of topical twists and turns, we bring you The Daily Lockdown&#39;s very own murder mystery! Don&#39;t forget to plan in advance so you can collect ingredients for your essential three-course meal in your once-weekly grocery shop. Costumes are a must! Now, time to get cracking!', 'NO'),
(3, 'Chloe', 'FaceWine', '2020-04-09', 'public/images/posts/FaceWine.jpg', 'public/images/posts/FaceWine2.jpg', 'Missing your evening tipple with your friends? We have the solution! Who says video call catch-ups have to be completely sober? We see no harm in raiding the back of your (probably slightly depleted) wine store. Maybe now is the time to pop open that bottle you were saving for a special occasion? Schedule a call with your favourites, grab a drink and chat, work your way through a quiz or share cocktail recipes whilst connected! Do it in pyjamas, or go wild and have some fun getting dressed up, it&#39;s truly up to you!', ''),
(4, 'Chloe', 'Bike Ride!', '2020-04-11', 'public/images/posts/BikeRide.jpeg', 'public/images/posts/BikeRide2.jpeg', 'Set off bright and early before the crowds have managed to leave their homes and plan a route that avoids potentially busy spaces.', 'YES'),
(5, 'Chloe', 'Make a scrapbook', '2020-04-01', 'public/images/posts/Makeascrapbook.jpeg', 'public/images/posts/Makeascrapbook2.jpeg', 'Order all your favourite photo prints online, and a week later you can get crafty!', 'NO'),
(6, 'Chloe', 'Get Quizzing!', '2020-04-22', 'public/images/posts/GetQuizzing.jpeg', 'public/images/posts/GetQuizzing2.jpeg', 'There&#39;s a whole world of live, interactive and recorded quizzes available right now! If you&#39;re feeling competitive, why not go head-to-head with your friends on a video call whilst you complete the same quiz against time? Ambitious? Create your own quiz!', 'NO'),
(7, 'Chloe', 'Organise', '2020-04-22', 'public/images/posts/Organise.jpeg', 'public/images/posts/Organise2.jpeg', 'If you were looking for a sign to sort out the messiest cupboard in your house - here it is! We&#39;re not forcing you to tidy but just imagine the feeling when your abandoned cupboard finally takes on an orderly appearance. Will you take on the challenge? We&#39;d love to see the results! Enjoy these calming images to stay composed as you deal with your collection of useless phone chargers and old manuals. ', 'NO'),
(18, 'Krystyna', 'Netflix and Chill', '2020-04-24', 'public/images/posts/NetflixandChill.jpeg', 'public/images/posts/NetflixandChill2.jpeg', 'Netflix and chill is not over.  We&#39;ve entered an unprecedented times thanks to the coronavirus pandemic and the ensuing need to practice social distancing. And with this unprecedented time has come unprecedented action. Netflix Party!&#13;&#10;Netflix Party is a Chrome extension for watching Netflix remotely with friends, e.g., for movie nights with that long-distance special someone. Enjoy!', 'NO'),
(19, 'Krystyna', 'Buckingham Palace Virtual Tour', '2020-04-24', 'public/images/posts/BuckinghamPalaceVirtualTour.jpeg', 'public/images/posts/BuckinghamPalaceVirtualTour2.jpeg', 'Many UK tourist attractions offer virtual tours, so you can look around from the comfort of your own home.&#13;&#10;The Buckingham Palace tour allows you to snoop around the Queen&#39;s house while learning about the history of the royal family.', 'NO'),
(20, 'Krystyna', 'Start a Blog', '2020-04-24', 'public/images/posts/StartaBlog.jpeg', 'public/images/posts/StartaBlog2.jpeg', 'You can never go wrong with writing a blog. Whether you&#39;re using it as an online writing portfolio, a space to share your passions or as an opportunity to try something new.&#13;&#10;&#13;&#10;It&#39;s time to get those creative juices flowing!', 'NO'),
(21, 'Krystyna', 'Upcycle your furniture ', '2020-04-24', 'public/images/posts/Upcycleyourfurniture.jpeg', 'public/images/posts/Upcycleyourfurniture2.jpeg', 'Can&#39;t stop staring at that annoying chip in your side board? Desperate to give those bookshelves a new lick of paint? Why not try your hand at a spot of DIY and transform those tired piece of furniture into something your friends will be jealous of. ', 'NO'),
(24, 'Gemma', 'The bake off', '2020-03-02', 'public/images/posts/Thebakeoff.jpeg', 'public/images/posts/Thebakeoff2.jpeg', 'Have each of your family members prepare a cake of their choice once a week. The first challenge is to have the goods last longer than a day! Second challenge is taste test! &#13;&#10;&#13;&#10;Get your family to each write a score out of 10 and put these scores out the way so no one can see. After you&#39;ve all given your best shot to avoid soggy bottoms - count up the scores and see who has won! &#13;&#10;&#13;&#10;Fun and tasty...&#13;&#10;', 'YES'),
(26, 'Gemma', 'Do some careful DIY', '2021-01-01', 'public/images/posts/DosomecarefulDIY.jpeg', 'public/images/posts/DosomecarefulDIY2.jpeg', 'You&#39;ve been putting it off for years, but now is the time to dig out your casual clothes. Spruce up your lounge, redo that paint job, finish those curtains. You&#39;ve got the time, you&#39;ve got the know how. Think of how impressed those house guests will be when you&#39;re allowed to invite people over again!', 'YES'),
(27, 'Gemma', 'Write a book', '2020-01-01', 'public/images/posts/Writeabook.jpeg', 'public/images/posts/Writeabook2.jpeg', 'We don&#39;t mean a novel...Write a 12 page book on a subject of your choice. Bring out that imaginative side!', 'NO'),
(28, 'Gemma', 'Cocktail', '2020-03-01', 'public/images/posts/Cocktail.jpeg', 'public/images/posts/Cocktail2.jpeg', 'Practice those cocktail making skills that you thought you had, but didn&#39;t have. This is your moment!', 'NO'),
(29, 'Gemma', 'Flower arranging', '2020-03-01', 'public/images/posts/Flowerarranging.jpeg', 'public/images/posts/Flowerarranging2.jpeg', 'Attempt to bring together two bouquets to be a beautiful masterpiece at your dinner table (or in the middle of the floor in my case...). As long as you have fun it&#39;s worth it!', 'NO'),
(30, 'Gemma', 'Bored or Board', '2020-01-01', 'public/images/posts/BoredorBoard.jpeg', 'public/images/posts/BoredorBoard2.jpeg', 'Dig out those Christmas board games and challenge your family to Monopoly... These games have a tendency to last for a while, and we certainly have time to kill! &#13;&#10;If you fancy a twist on the norm, you could download an app game. Try Words with Friends or Skribble if you are isolated from others. There is never a better moment to skill up!', 'NO'),
(33, 'Gemma', 'Learn a new skill', '2020-03-02', 'public/images/posts/Learnanewskill.jpeg', 'public/images/posts/Learnanewskill2.jpeg', 'With all this time at home you may as well learn that language that you&#39;ve been meaning to try. A fan of cider? Why not pick up a course about your favourite tipple. Pick a subject you&#39;re interested in and it&#39;ll keep you occupied for hours!', 'NO'),
(35, 'Gemma', 'Nail Art!', '2020-04-03', 'public/images/posts/NailArt.jpeg', 'public/images/posts/NailArt2.jpeg', 'Just because you&#39;re stuck inside doesn&#39;t mean you need to let yourself go!&#13;&#10;&#13;&#10;Give yourself a manicure and practice some cool art designs! There are plenty of tutorials on youtube. Go dotty, floral, try out animal print or even a mini scene! Then instagram those beauties and show off your new style.&#13;&#10;&#13;&#10;Don&#39;t forget to not touch anything afterwards so you do not smudge your masterpiece!&#13;&#10;&#13;&#10;Missing nights in with the girls? Arrange to meet them on zoom whilst you doll up. PJ&#39;s encouraged.&#13;&#10;&#13;&#10;Nothing better than a Nails, Zoom and Wine night!', 'NO'),
(36, 'Gemma', 'Get planning', '2020-04-03', 'public/images/posts/Getplanning.jpeg', 'public/images/posts/Getplanning2.jpeg', 'So you&#39;re not off on your favourite adventures this year, but don&#39;t despair. This year is the year for planning! Make a to-do jar of all those places you want to visit and activities you want to partake in. Start off with places in your home country for when lockdown is lifted and work your way up to those more exotic places. The world is beautiful and it will be blossoming in time for our release!', 'NO'),
(37, 'Gemma', 'Learn to sew', '2020-04-04', 'public/images/posts/Learntosew.jpeg', 'public/images/posts/Learntosew2.jpeg', 'With a couple of months at home you could be upcycling all your old gear and creating the most fabulous outfits. You&#39;ll be receiving all the compliments and be able to reply &#34;Oh this? No it&#39;s not ASOS. I made it!&#34;.  You&#39;ve got plenty of time to be the best dressed for Autumn Winter! Get sewing!', 'NO'),
(38, 'Gemma', 'Fancy dress fun', '2020-03-05', 'public/images/posts/Fancydressfun.jpeg', 'public/images/posts/Fancydressfun2.jpeg', 'There is something that always puts a smile on my face when I&#39;m preparing for a fancy dress party. How about starting preparations for Halloween early this year and creating an ingenious costume! You&#39;ll be the start of the show. &#13;&#10;&#13;&#10;Don&#39;t fancy dressing up yourself? How about creating your pet an outfit? In most cases they love the attention. &#13;&#10;&#13;&#10;DISCLAIMER: If your pet is not one of those whom loves dressing up, please do not force them as this is cruel.', 'NO'),
(40, 'Krystyna', 'Draw', '2020-03-02', 'public/images/posts/Draw.jpeg', 'public/images/posts/Draw2.jpeg', 'Practice some drawing or colouring to relax', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userEmail` varchar(30) NOT NULL,
  `password` varchar(80) NOT NULL,
  `author` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userEmail`, `password`, `author`) VALUES
('chloe@lockdown.com', '$2y$10$3GKEcawurPWAD167ORXMWuxNz/Wzj62c2wlXd8GVhDHum6D7rLK9a', 'Chloe'),
('gemma@lockdown.com', '$2y$10$K4h0YhNKn.iPE.L9jVVKkOwWLeIpRGmywrhHdR2FuZ.0I7vAUEesC', 'Gemma'),
('krystyna@lockdown.com', '$2y$10$4y0eNMWeT/0tOz3ajOGoPe1TtdBB1uNwXTIPkVmoYT8WdfvtEWPm2', 'Krystyna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`commentID`),
  ADD KEY `postID` (`postID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`postID`),
  ADD KEY `author` (`author`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`author`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `postID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`author`) REFERENCES `user` (`author`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
