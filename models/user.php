<?php

class User {
    public $userEmail;
    public $password;
    public $author;

    public function __construct($userEmail, $password, $author) { 
      $this->userEmail = $userEmail;
      $this->password = $password;
      $this->author = $author;
    }
public static function find($userEmail, $password){
         $db = Db::getInstance();
         $req = $db->prepare('SELECT * FROM user WHERE userEmail=:userEmail');
         if (isset($_POST['userEmail'])&& $_POST['userEmail'] != ""){
             $filteredEmail = filter_input(INPUT_POST, 'userEmail', FILTER_SANITIZE_EMAIL);
         } 
         $req->bindParam('userEmail', $_POST['userEmail']);
         $req->execute();
         $found = $req->fetch();
         if($found){
            return new User ($found['userEmail'], $found['password'], $found['author']); 
         }
         else {
            echo '<div class="container">
                <div class="break row justify-content-center">
                <div class="col-md-4"><h4> Please enter correct login details. </h4> </div></div></div>';
            require_once('views/users/login.php'); 
         }
    }
}
?>

