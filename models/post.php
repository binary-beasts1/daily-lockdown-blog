<?php

class Post {

    public $postID;
    public $title;
    public $postcontent;
    public $published;
    public $feature;
    public $author;
    public $image1;
    public $image2;

    public function __construct($postID, $title, $postcontent, $published, $feature, $author, $image1, $image2) {
        $this->postID = $postID;
        $this->title = $title;
        $this->postcontent = $postcontent;
        $this->published = $published;
        $this->feature = $feature;
        $this->author = $author;
        $this->image1 = $image1;
        $this->image2 = $image2;
    }

    public static function all() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT postID, title, postcontent, published, feature, author, image1, image2 FROM post ORDER BY published DESC');
        foreach ($req->fetchAll() as $post) {
            $list[] = new Post($post['postID'], $post['title'], $post['postcontent'], $post['published'], $post['feature'], $post['author'], $post['image1'], $post['image2']);
        }
        return $list;
    }

    public static function count() {
        $db = Db::getInstance();
        foreach ($db->query("SELECT COUNT(*) FROM post WHERE feature = 'YES'") as $row) {
            return $count = $row['COUNT(*)'];
        }
    }

    public static function featall() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT post.postID, post.title, post.postcontent, post.published, post.feature, user.author, post.image1, post.image2 FROM post JOIN user ON post.author=user.author WHERE post.feature=1 ORDER BY post.postID DESC LIMIT 3');
        foreach ($req->fetchAll() as $feat) {
            $list[] = new Post($feat['postID'], $feat['title'], $feat['postcontent'], $feat['published'], $feat['feature'], $feat['author'], $feat['image1'], $feat['image2']);
        }
        return $list;
    }
     public static function featallcheck() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT post.postID, post.title, post.postcontent, post.published, post.feature, user.author, post.image1, post.image2 FROM post JOIN user ON post.author=user.author WHERE post.feature=1 ORDER BY post.postID DESC');
        foreach ($req->fetchAll() as $feat) {
            $list[] = new Post($feat['postID'], $feat['title'], $feat['postcontent'], $feat['published'], $feat['feature'], $feat['author'], $feat['image1'], $feat['image2']);
        }
        return $list;
    }
    public static function find($postID) {
        $db = Db::getInstance();
        $postID = intval($postID); 
        $req = $db->prepare('SELECT postID, title, postcontent, published, feature, author, image1, image2 FROM post WHERE postID=:postID');
        $req->execute(array('postID' => $postID));
        $post = $req->fetch();
        if ($post) {
            return new Post($post['postID'], $post['title'], $post['postcontent'], $post['published'], $post['feature'], $post['author'], $post['image1'], $post['image2']);
        } else {
            throw new Exception('Your post could not be found');
        }
    }

    public static function paginationselect() {
        $db = Db::getInstance();
        $search_keyword = '';
        if (!empty($_POST['search']['keyword'])) {
            $search_keyword = $_POST['search']['keyword'];
        }
        $sql = 'SELECT post.postID, post.title, post.postcontent, post.published, post.feature, user.author, post.image1, post.image2 FROM post JOIN user ON post.author=user.author WHERE title LIKE :keyword OR postcontent LIKE :keyword OR published LIKE :keyword ORDER BY postID DESC ';
      


        $per_page_html = '';
        $page = 1;
        $start = 0;
        if (!empty($_POST["page"])) {
            $page = $_POST["page"];
            $start = ($page - 1) * 5;
        }
        $limit = " limit " . $start . "," . 5;
        $pagination_statement = $db->prepare($sql);
        $pagination_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
        $pagination_statement->execute();

        $row_count = $pagination_statement->rowCount();
        if (!empty($row_count)) {
            $per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
            $page_count = ceil($row_count / 5);
            if ($page_count > 1) {
                for ($i = 1; $i <= $page_count; $i++) {
                    if ($i == $page) {
                        $per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page current" />';
                    } else {
                        $per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page" />';
                    }
                }
            }
            $per_page_html .= "</div>";
        }

        $query = $sql . $limit;
        $pdo_statement = $db->prepare($query);
        $pdo_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
        $pdo_statement->execute();
         foreach ($pdo_statement->fetchAll() as $pagpost) 
             {
            $list[] = new Post($pagpost['postID'], $pagpost['title'], $pagpost['postcontent'], $pagpost['published'], $pagpost['feature'], $pagpost['author'], $pagpost['image1'], $pagpost['image2']);
        }
        return $list;
        
    }

    public static function update($postID) {
        $db = Db::getInstance();
        $req = $db->prepare("UPDATE post SET title=:title, postcontent=:postcontent, published=:published, feature=:feature, author=:author, image1=:image1, image2=:image2 where postID=:postID");
        $req->bindParam(':postID', $postID);
        $req->bindParam(':title', $title);
        $req->bindParam(':postcontent', $postcontent);
        $req->bindParam(':published', $published);
        $req->bindParam(':feature', $feature);
        $req->bindParam(':author', $author);

        if (isset($_POST['title']) && $_POST['title'] != "") {
            $filteredTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_SPECIAL_CHARS);
        }

//replacing characters for the title of image

        $str = ['!', ':', '?', ' ', ';'];
        $rplc = ['', '', '', '', ''];

        $trimmed = str_replace($str, $rplc, $filteredTitle);

        $path = "public/images/posts/";
        $file = $path . $trimmed . '.jpeg';
        $file2 = $path . $trimmed . '2.jpeg';
        if (isset($path, $file, $file2)) {
            $req->bindParam(':image1', $file);
            $req->bindParam(':image2', $file2);
        } else {
            echo 'There has been an error in building a new filepath for your image';
        }

        if (isset($_POST['postcontent']) && $_POST['postcontent'] != "") {
            $filteredPostcontent = filter_input(INPUT_POST, 'postcontent', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['published']) && $_POST['published'] != "") {
            $filteredPublished = filter_input(INPUT_POST, 'published', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['feature']) && $_POST['feature'] != "") {
            $filteredFeature = filter_input(INPUT_POST, 'feature', FILTER_SANITIZE_SPECIAL_CHARS);
             $feature = $filteredFeature;
        }
        if (isset($_POST['author']) && $_POST['author'] != "") {
            $filtereduser = filter_input(INPUT_POST, 'author', FILTER_SANITIZE_SPECIAL_CHARS);
        }

        $title = $filteredTitle;
        $postcontent = $filteredPostcontent;
        $published = $filteredPublished;
        $author = $filtereduser;
        $req->execute();

        Post::uploadFile($trimmed);
    }

    public static function add() {
        $db = Db::getInstance();
        $req = $db->prepare("Insert into post(title, postcontent, published, feature, author, image1, image2) values (:title, :postcontent, :published, :feature, :author, :image1, :image2)");
        $req->bindParam(':title', $title);
        $req->bindParam(':postcontent', $postcontent);
        $req->bindParam(':published', $published);
        $req->bindParam(':feature', $feature);
        $req->bindParam(':author', $_SESSION['author']);

//replacing characters for the title of image
        $str = ['!', ':', '?', ' ', ';'];
        $rplc = ['', '', '', '', ''];

        $trimmed = str_replace($str, $rplc, $_POST['title']);

//upating file path
        $path = "public/images/posts/";
        $file = $path . $trimmed . '.jpeg';
        $file2 = $path . $trimmed . '2.jpeg';
        if (isset($path, $file, $file2)) {
            $req->bindParam(':image1', $file);
            $req->bindParam(':image2', $file2);
        } else {
            echo 'There has been an error in building a new filepath for your image';
        }

// set name and price parameters and execute
        if (isset($_POST['title']) && $_POST['title'] != "") {
            $filteredTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['postcontent']) && $_POST['postcontent'] != "") {
            $filteredPostcontent = filter_input(INPUT_POST, 'postcontent', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['published']) && $_POST['published'] != "") {
            $filteredPublished = filter_input(INPUT_POST, 'published', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['feature']) && $_POST['feature'] != "") {
            $filteredFeature = filter_input(INPUT_POST, 'feature', FILTER_SANITIZE_SPECIAL_CHARS);
        }

        Post::uploadFile($trimmed);

        $title = $filteredTitle;
        $postcontent = $filteredPostcontent;
        $published = $filteredPublished;
        $feature = $filteredFeature;
        $req->execute();
    }



    const AllowedTypes = ['image/jpeg', 'image/jpg', 'image/JPG', 'image/JPEG', 'image/png', 'image/PNG'];
    const InputKey = "uploader";
    const InputKey2 = "uploader2";


    public static function uploadFile(string $name) {

        $path = "C:/xampp/htdocs/DailyLockdown/public/images/posts/";
        
        if(isset($_FILES[self::InputKey])){
        $tempFile = $_FILES[self::InputKey]['tmp_name'];
        $destinationFile = $path . $name . '.jpeg';
        if (!move_uploaded_file($tempFile, $destinationFile)) {
            if(empty($_FILES[self::InputKey])){
            trigger_error("Unable to upload file");}
        }
        if (file_exists($tempFile)) {
            unlink($tempFile);
        }
        }   
        if(isset($_FILES[self::InputKey])){
        $tempFile2 = $_FILES[self::InputKey2]['tmp_name'];
        $destinationFile2 = $path . $name . '2.jpeg';
        if (!move_uploaded_file($tempFile2, $destinationFile2)) {
            if(empty($_FILES[self::InputKey])){
            trigger_error("Unable to upload file");}
        }
//Clean up the temp file
        
        if (file_exists($tempFile2)) {
            unlink($tempFile2);
        }}
    }

    public static function remove($postID) {
        $db = Db::getInstance();
        $postID = intval($postID);
        $req = $db->prepare('DELETE FROM post WHERE postID = :postID');
        $req->execute(array('postID' => $postID));
    }
    
    public static function nextpage($postID){
	$db = Db::getInstance();
	$postID = intval($postID);
	$req = $db->prepare('SELECT postID FROM post');
	$req->execute();
	$allID = $req->fetchAll(\PDO::FETCH_ASSOC);
	$key = array_search($postID, array_column($allID, 'postID'));
        $newkey = $key +1;
	
        if(empty($allID[($newkey)])){
        $nextpage = min($allID);}
        else{
        $nextpage = $allID[($newkey)];}
        return implode(array_values($nextpage));
    }
    
    public static function previouspage($postID){
	$db = Db::getInstance();
	$postID = intval($postID);
	$req = $db->prepare('SELECT postID FROM post');
	$req->execute();
        $allID = $req->fetchAll(\PDO::FETCH_ASSOC);
	$key = array_search($postID, array_column($allID, 'postID'));
        $newkey = $key -1;
        if(empty($allID[($newkey)])){
        $previouspage = max($allID);}
        else{
        $previouspage = $allID[($newkey)];}
        return implode(array_values($previouspage));
        }
    }

class Comment {

    public $postID;
    public $commentID;
    public $content;
    public $guestEmail;
    public $guestName;

    public function __construct($postID, $commentID, $content, $guestEmail, $guestName) {
        $this->postID = $postID;
        $this->commentID = $commentID;
        $this->content = $content;
        $this->guestEmail = $guestEmail;
        $this->guestName = $guestName;
    }

    public static function findCommentsPostId($postID) {
        $comments = [];
        $db = Db::getInstance();
        $postID = intval($postID); // no need to further sanitize - verifies that the postID is an integer
        $req = $db->prepare('SELECT * FROM comment WHERE postID=:postID');
        $result = $req->execute(array('postID' => $postID));
        foreach ($req->fetchAll() as $comment) {
            $comments[] = new Comment($comment['postID'], $comment['commentID'], $comment['content'], $comment['guestName'], $comment['guestEmail']);
        }
        return $comments;
    }

    public static function addComment($postID) {

        $db = Db::getInstance();
        $req = $db->prepare("Insert into comment(content, postID, guestName, guestEmail) values (:content, :postID, :guestName, :guestEmail)");
        $postID = intval($postID);
        $req->bindParam(':content', $content);
        $req->bindParam(':postID', $postID);
        $req->bindParam(':guestName', $guestName);
        $req->bindParam(':guestEmail', $guestEmail);

        if (isset($_POST['content']) && $_POST['content'] != "") {
            $filteredContent = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['guestName']) && $_POST['guestName'] != "") {
            $filteredName = filter_input(INPUT_POST, 'guestName', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (isset($_POST['guestEmail']) && $_POST['guestEmail'] != "") {
            $filteredEmail = filter_input(INPUT_POST, 'guestEmail', FILTER_SANITIZE_SPECIAL_CHARS);
        }

        $content = $filteredContent;
        $guestName = $filteredName;
        $guestEmail = $filteredEmail;
        $req->execute();
    }

    public static function remove($commentID) {
        $db = Db::getInstance();
        $commentID = intval($commentID);
        $req = $db->prepare('DELETE FROM comment WHERE commentID = :commentID');
        $req->execute(array('commentID' => $commentID));
    }
   
}
?>

