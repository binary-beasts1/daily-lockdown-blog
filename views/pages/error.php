<div class="break container">
            <div class="row">
                <div class="mb-4  mb-md-0 col-md-6 align-self-center">
                            <h1 class="blog-heading">Oops, something's gone wrong!</h1>
                            <p>Not to worry, why don't you try one of these helpful links:</p>
                            <a href='index.php' class="btn button error-button">HOME</a>
                            <a href='about.php' class="btn button">ABOUT</a>
                </div>
                <div class="error-margin col-md-6 align-self-center">           
                   <img class='img-fluid' src="public/images/error2.jpg">
                </div>
                
            </div>
             </div>


          <!--FOOTER-->
    <footer>
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
            <form id="admin-form">
                <p>Admin use only:</p>
                
                  <a href='?controller=user&action=login' class="btn button" name="login">LOGIN</a>
            </form>
            </div>
        </div>
        </div>
    </footer>