<!--Hero Image-->        
<div class="bg-image jumbotron jumbotron-fluid">
    <div class="relative container">
        <div class="text-box">
            <h1 class="card-title blog-heading text-uppercase">About</h1>
            <div class="card-text">
                <p>Hello there! We're a group of humans, all trapped at home. Sound Familiar? Our aim is to add a little life back into your corona-existence. We work together (remotely of course) to bring you activities to keep you sane.</p>
                <p>Welcome to our <b>boredom-busting</b> blog!</p>
            </div>
        </div>
    </div>
</div> 


<!--MAIN--> 
<div class="container">
    <p>Thanks for visiting our blog! We hope you can find something to keep you entertained amongst our many ideas! On this page you can find out about our origins, ideas, and the people behind all of this! 
        <br/>
    <p><b>We have the answers! Browse, read and bust that boredom!</b></p>
</div>

<div class="container">

    <div class="break row">
        <div class="col-md-6 align-self-center">
            <div class='post-text'>
                <h2 class="text-uppercase">The beginning</h2>
                <p>This adventure began at the beginning of April 2020. This site was born out of a project specification for a blog of our choosing to present at the end of our challenging and exciting journey with Get Into Tech at Sky. Who knew you could learn this much in just 14 weeks?</p>

                    </div>
                </div>
                
                <div class="col-md-6 align-self-center">
                    <img class="img-fluid" src="public/images/team.jpg">
                </div>
            </div>
            
            
            
        <!--Author Profiles-->
        <h2 class="break text-uppercase">Meet the Binary Beasts!</h2>

        <div id="authors" class="row">
        <div class="col-lg-4 d-flex align-items-stretch">
         <div class="card-margin card mx-auto">
             <img src="public/images/krystyna.jpg" class="card-img-top mx-auto" alt="">
             <div class="card-body">
              <h5 class="card-title">Krystyna</h5>
              <p class="card-text">This project's front-end superstar! What you're seeing here is either made or inspired by her brilliant taste and skills in design.</p>
             </div>
         </div>
            </div>
             
         <div class="col-lg-4 d-flex align-items-stretch">
         <div class="card-margin card mx-auto">
             <img src="public/images/gem.jpg" class="card-img-top mx-auto" alt="">
             <div class="card-body">
             <h5 class="card-title">Gemma</h5>
              <p class="card-text">Our git pro has us all organised and running smoothly! The project would fall apart without Gemma working hard to teach us all the world of pull, add, commit and push. </p>
             </div>
         </div>
             </div>
        
         <div class="col-lg-4 d-flex align-items-stretch">
         <div class="card-margin card  mx-auto">
             <img src="public/images/chloeee.jpg" class="card-img-top mx-auto" alt="">
             <div class="card-body">
                 <h5 class="card-title">Chloe</h5>
                <p class="card-text">The third member of the team has our database queries running like clockwork! What's a blog without content?</p>
             </div>
         </div> 
         </div>
        </div>
    

    <!--Contact us-->
    <div class="row">   
        <div class="col-md-6 align-self-center">
            <img class="img-fluid" src="public/images/contact.jpg">
        </div>
        <div class="col-md-6 align-self-center">
            <div class='post-text'>
                <h2 class="text-uppercase">Contact us</h2>
                <p>Want to say hello? Have suggestions? Send us an <a href='mailto:creators@dailylockdown.com'>email</a>! We would love hear from you!</p>
            </div>
        </div>
    </div>
</div>

<!--FOOTER-->
<footer>
    <hr>
    <div class="container">
        <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>


             <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
            <form id="admin-form">
                <?php if(!isset($_SESSION['author'])) { 
                    echo '<p>Admin use only:</p> <a href="?controller=user&action=login" class="btn button" name="login">LOGIN</a>';
                }
                if(isset($_SESSION['author'])) { 
                    echo '<a href="?controller=user&action=logout" class="btn button" name="logout">LOGOUT</a>'; 
                    
                } ?>
              </form>
            </div>
        </div>
    </div>
</footer>
