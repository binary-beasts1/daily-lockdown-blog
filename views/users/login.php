
<div class="container">
    <div class="break row justify-content-center">
        <div class="col-md-4">
            <h3 class="mb-3">Login</h3>
            <form name="frmAdd"  action="" method="POST">
                <div class="form-group">
                    <input type="text" name="userEmail" class="demo-form-field form-control form-control-lg" placeholder="email@email.com" required  />
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="demo-form-field form-control form-control-lg" placeholder="password" required />
                </div>
                <div class="form-group">
                    <button type="submit" name="login" class="btn button">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

  <!--FOOTER-->
  <footer>
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                <p class="break-share">Welcome! Please log in.</p>
            </div>
        </div>
        </div>
    </footer>