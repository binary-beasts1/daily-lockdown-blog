<div class="container">
    <div class="break row justify-content-center">
        <div class="col-md-5">
            <h4 class="mb-3">Hello <?php echo $_SESSION['author'];?>! You have successfully logged in.</h4>
            <br/>
             <a href='?controller=post&action=readAll ' class="btn button">View All Posts</a> 
                </div>
        </div>
    </div>

  <!--FOOTER-->
    <footer>
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                <a href="index.php" class="btn button" name="login">LOGOUT</a>
            </div>
        </div>
        </div>
    </footer>

