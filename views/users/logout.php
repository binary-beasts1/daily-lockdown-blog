<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="break row justify-content-center">
        <div class="col-md-5">
            <h4 class="mb-3">You have successfully logged out.</h4>
            <br/>
             <a href='index.php' class="btn button">Back to the homepage</a> 
                </div>
        </div>
    </div>
</body>

  <!--FOOTER-->
    <footer class="fixed-bottom">
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                <a href='?controller=user&action=login' class="btn button" name="login">LOGIN</a>
            </div>
        </div>
        </div>
    </footer>
</html>
   <script type="text/javascript">
  $(document).ready(function() {
      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
          window.history.pushState(null, "", window.location.href);
      };
  });
</script>