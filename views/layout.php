<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta description="The blog that will make your life easier during the pandemic times.">
        <title>The Daily Lockdown</title>
        <link rel="stylesheet" href="public/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="public/css/styles.css" type="text/css">
        <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e9ed92b81693d0012e58860&product=image-share-buttons&cms=website' async='async'></script>
        <link rel="script" href="public/js/script.js" type="script">
    </head>
  <body>
  <!--NAVBAR-->
   <div class="container-fluid">
     <nav class="navbar navbar-expand-lg navbar-light">
            <a href="index.php" class="navbar-brand"><img src="public/images/Logo.png" width="60%"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>  
        
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link menu" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu" href="?controller=pages&action=about">About</a>
                </li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="#" target="_blank" rel="noopener"><img class="nav-social" src="public/images/instagram.png" alt="Instagram social icon"></a></li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="#" target="_blank" rel="noopener"><img class="nav-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a></li>
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="#" target="_blank" rel="noopener"><img class="nav-social" src="public/images/twitter.png" alt="Twitter social icon"></a></li>
            </ul>
        </div>
      </nav>
       
        </div>
        <hr>

<div>
    <?php require_once('routes.php'); ?>
</div>
        <a href="#" class="back-to-top"><svg class="bi bi-eject-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M7.27 1.047a1 1 0 011.46 0l6.345 6.77c.6.638.146 1.683-.73 1.683H1.656C.78 9.5.326 8.455.926 7.816L7.27 1.047zM.5 11.5a1 1 0 011-1h13a1 1 0 011 1v1a1 1 0 01-1 1h-13a1 1 0 01-1-1v-1z" clip-rule="evenodd"/>
</svg></a>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
                   jQuery(document).ready(function() {
        var offset = 220;
        var duration = 500;
    jQuery(window).scroll(function() {

        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
    });
				
    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
    });
  </script>
  </body>
</html>
