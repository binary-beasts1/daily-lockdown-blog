<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function(){
    var maxLength = 200;
    $(".show-read-more").each(function(){
        var myStr = $(this).text();
        if($.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(newStr);
            $(this).append(' <a href="javascript:void(0);" class="read-more">READ MORE...</a>');
            $(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    $(".read-more").click(function(){
        $(this).siblings(".more-text").contents().unwrap();
        $(this).remove();
    });
});
</script>
<style>
    .show-read-more .more-text{
        display: none;
    }
</style>
<!--POSTS--> 
 <!-- admin logout reminder -->
<div class ="break container text-center">
<?php if(isset($_SESSION['author'])){
    echo '<h3>'. $_SESSION['author'].' you are still logged in! </h3>'.'<p><a href="?controller=user&action=logout" class="btn button" name="login">LOGOUT</a></p>'; } ?>
</div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-2 order-md-1 align-self-center ">
                   <?php try {?>
                    <img class=" img-fluid" src="<?php echo $post->image1 ?>">
                        <?php } catch (error $e){ ?> <img class="img-fluid" src="views/public/images/errorimg2.JPG"> <?php echo $e->getMessage() . '@' . $e->getLine(); } ?>
                </div> <!-- this does not work --> 
                
                <div class="col-md-6 order-1 order-md-2 align-self-center">
                        <div class="post-text">
                            <h1 class="blog-heading text-uppercase"><?php echo $post->title; ?></h1>
                            
                        </div>
                </div>
                
            </div>
             </div>
             
           <div class="container">
            <div class="break row">
                <div class="col-md-6 align-self-center">
                        <div class="post-text">
                            
                            <p><?php echo $post->postcontent; ?></p>
                        </div>
                </div>
                
                <div class="col-md-6 order-1 order-md-2 align-self-center">
                    <?php try {?>
                    <img class="img-fluid" src="<?php echo $post->image2 ?>">
                    <?php } catch (error $e){ ?> <img class="img-fluid" src="views/public/images/errorimg2.JPG"> <?php echo $e->getMessage() . '@' . $e->getLine(); } ?>
                </div>
            </div>
               
<!----Next and Previous buttons-->
               <div class="break-search row">
                   <div class="col-sm-6 np-btn">
                   <a href='?controller=post&action=comRead&postID=<?=$previous; ?>' role="button" class="btn button-post"><svg class="bi bi-caret-left-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 00-1.659-.753l-5.48 4.796a1 1 0 000 1.506z"/>
                   </svg>PREVIOUS POST</a>  
                   </div>
                    <div class="col-sm-6 text-right np-btn">
                   <a href='?controller=post&action=comRead&postID=<?=$next; ?>' role="button" class="btn button-post">NEXT POST<svg class="bi bi-caret-right-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 011.659-.753l5.48 4.796a1 1 0 010 1.506z"/></svg></a>
                   </div>
                </div>
            </div>
 
 
      
            <div class="b-color container-fluid">
            <div class="break-search row container mx-auto"> 
                <div class="mx-auto col-md-12 align-self-center blog-tc">
                    <h5>We'd love to see how you're keeping busy in the lockdown! Tag us on Instagram or Twitter @TheDailyLockdown</h5>
                </div>
                
            </div>
            </div>

        
    <!--COMMENTS SECTION--> 
        <div class="container">
            <div class="break break-comments row">
            <div class="col-md-7">
                    <p class="title-comments"><svg id="chat-box" class="bi bi-chat-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
                  
                        
                        
                        </svg>COMMENTS</p>
                
                <?php foreach($comments as $comment) { ?>
 
                <div class="box-comments">
                <p><span class="button-post"><?php echo $comment->guestEmail; ?></span> SAYS:</p>
                <p class="show-read-more"><?php echo $comment->content; ?></p>
                </div>
                <?php } ?>
                <div>
                    <p class="title-comments">Leave a Commment</p>
                    <p>Your email address will not be published.</p>
                    <form action="" method="POST">
                      <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="Let's Chat!" name="content" value="" required></textarea>
                      </div>
                          <div class="form-row">
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Name" name="guestName" value="" maxlength="25" required>
                            </div>
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Email" name="guestEmail" value="" maxlength="254" required>
                            </div>
                              <div>
                                  <input type="hidden" class="" name="postID" value="<?php echo $_GET['postID'];?>">
                              </div>
                          
                          </div>
                        <br/>
                    <button type="submit" class="btn button-post" name="add_comment" value="Add a Comment"><svg class="bi bi-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                      <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"/>
                    </svg>Add a Comment</button>
                    </form>
                </div>
            </div>
            
            
            <div class="col-comments col-md-4">
                <p class="title-comments">SHARE</p>
                <div class="break-share">
                <div class="sharethis-inline-share-buttons"></div>
                </div>
                <p class="break-share">Written by: <span class="button-post"><?php echo $post->author;?></span></p>
                <p>Published Date: <span class="button-post"><?php echo $post->published; ?></span></p>
            </div>
           </div>
        </div>
    
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
            
            $(".img").on("error",function(){
                alert(Handler for error called)})
                    .attr("src", 'views/images/errorimg2.JPG');
            
            
            </script>
   
   
         <!--FOOTER-->
    <footer>
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
            <form id="admin-form">
                <p>Admin use only:</p>
                  <a href='?controller=user&action=login' class="btn button" name="login">LOGIN</a>
            </form>
            </div>
        </div>
        </div>
    </footer>
