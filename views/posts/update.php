<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    
    //error checking pre-uploaded
    $(document).ready(function(){
        $("#Input1").change(function(){
          
          var fileExtension = ['jpeg', 'jpg'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
            $("#submit").prop('disabled', true); 
         //   $("h6").text("Please choose a valid first image before you can submit.");
            $("#imgtype").html("<h6>Please choose a valid first image before you can submit.</h6>");
        } else {
            $("#submit").prop('disabled', false); 
            $("#imgtype").html("<h6></h6>");
        }
        });
    });
    
            $(document).ready(function(){
        $("#Input2").change(function(){
          
          var fileExtension = ['jpeg', 'jpg'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
            $("#submit").prop('disabled', true); 
            $("#imgtype2").html("<h6>Please choose a valid second image before you can submit.</h6>");
        } else {
            $("#submit").prop('disabled', false); 
            $("#imgtype2").html("<h6></h6>");
        }
        });
    });
    
</script>
<?php if(!empty($_SESSION['email'])){ ?>
<div class="break container">
    <a href='?controller=post&action=readAll' class="btn button" name="back">Back to All Posts</a>
</div>

<div class="container">
    <div class="break row justify-content-center">
        <div class="col-md-8">
            <h2 class="mb-3">UPDATE YOUR POST</h2>
            <form name="frmAdd"  action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for='title'>Title</label>
                    <input type="text" name="title" class="demo-form-field form-control form-control-lg" placeholder="Witty title here!" value="<?= $post->title; ?>" required  />
                </div>
                <div class="form-group">
                    <label for='content'>Content</label>
                    <textarea name="postcontent" class="demo-form-field form-control form-control-lg" rows="10" placeholder="Input your blog post here!" required><?= $post->postcontent; ?></textarea>
                </div>
               
                <div class="form-group">
                    <label for='published'>Published Date</label>
                    <input type="date" name="published" class="demo-form-field form-control form-control-lg" value="<?= $post->published; ?>" required />
                </div>
                <div class="form-group">
                    <label for='feature'>Featured Post on Homepage?</label> <br>
                    <input type="radio" id="YES" name="feature" value='YES' required <?php $check = $post->feature;
if ($check == 'YES') {
    echo 'checked';
} ?>>
                    <label for="YES">Yes, I'd like this post to be a featured post!</label><br>
                    <input type="radio" id="NO" name="feature" value='NO' required <?php $check = $post->feature;
if ($check == 'NO') {
    echo 'checked';
} ?>>
                    <label for="NO">No, I would not like this to be a featured post.</label>
             </div>
                <div class="form-group">
                    <label for='author'>Author</label>
                    <input type="text" name="author" class="demo-form-field form-control form-control-lg" placeholder="Author" value="<?= $post->author; ?>" required  />
                </div>

                <!--images-->
                    <div class="form-group">
                        <p>First Image</p>
                        
                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        <input id="Input1" type="file" name="uploader" />
                        <?php
                        $file = $post->image1;

                        if (!empty($file)) {
                            echo "<p>Your first image is</p><img src='$file' width='150'  alt=$file>";
                        } else {
                            
                            echo "<p> No image in this position in the post! <p/>";
                        }
                        ?>
                    </div>
                    
                   <div class="form-group">
                        <p>Second Image</p>
                        
                         <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                        <input id="Input2" type="file" name="uploader2"/>
                        <?php
                        $file2 = $post->image2;
                        if (!empty($file2)) {
                            echo "<p>Your second image is</p> <img src='$file2' width='150' alt=$file2>";
                        } else {
                            echo "<p> No image in this position in the post! <p/>";
                        }
                        ?>
                       

                    </div>

                <button id="submit" href="#" type="submit" name="save_record" class="btn button-post mt-3"><svg class="bi bi-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"/>
                    </svg>Update</button>
                
                <!--jquery-->
                                        <div  id="imgtype"></div>
                                        <div  id="imgtype2"></div>
                
            </form>
        </div>
    </div>
</div>

<!--FOOTER-->
<footer>
    <hr>
    <div class="container">
        <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                <p class="break-share">Welcome! Your are logged in as Admin.</p>
                <a href="index.php" class="btn button" name="login">LOGOUT</a>
            </div>
        </div>
    </div>
    <?php } else{require_once 'index.php';}?>
    <script>
        src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js";
        $('.featured').each(function(){
        if ($(this).val() == 1){
        $(this).attr("checked", "checked");

    </script>    

</footer>
