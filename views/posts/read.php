<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function(){
    var maxLength = 200;
    $(".show-read-more").each(function(){
        var myStr = $(this).text();
        if($.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(newStr);
            $(this).append(' <a href="javascript:void(0);" class="read-more">READ MORE...</a>');
            $(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    $(".read-more").click(function(){
        $(this).siblings(".more-text").contents().unwrap();
        $(this).remove();
    });
});
</script>
<style>
    .show-read-more .more-text{
        display: none;
    }
</style>
<!--POSTS-->  
<?php if(!empty($_SESSION['email'])){ ?>
  <div class="break container">
    <a href='?controller=post&action=readAll' class="btn button" name="back">Back to All Posts</a>
    <div><br/></div>
    <p>This is how a user would see your post</p>
</div>

        <div class="break container">
            <div class="row">
                <div class="col-md-6 order-2 order-md-1 align-self-center">   
                    <?php try {?>
                    <img class=" img-fluid" src="<?php echo $post->image1 ?>">
                        <?php } catch (error $e){ ?> <img class="img-fluid" src="views/public/images/errorimg2.JPG"> <?php echo $e->getMessage() . '@' . $e->getLine(); } ?>
                </div>
                
                <div class="col-md-6 order-1 order-md-2 align-self-center">
                        <div class="post-text">
                            <h1 class="blog-heading text-uppercase"><?php echo $post->title; ?></h1>
                            
                        </div>
                </div>
                
            </div>
             </div>
             
           <div class="container">
            <div class="break row">
                <div class="col-md-6 align-self-center">
                        <div class="post-text">
                            <p><?php echo $post->postcontent; ?></p>
                        </div>
                </div>
                
                <div class="col-md-6 order-1 order-md-2 align-self-center">
                    <?php try {?>
                    <img class="img-fluid" src="<?php echo $post->image2 ?>">
                    <?php } catch (error $e){ ?> <img class="img-fluid" src="views/public/images/errorimg2.JPG"> <?php echo $e->getMessage() . '@' . $e->getLine(); } ?>
                    
                </div>
            </div>
            </div>
            
            <div class="b-color container-fluid">
            <div class="break row container mx-auto"> 
                <div class="mx-auto col-md-12 align-self-center blog-tc">
                    <h5>We'd love to see how you're keeping busy in the lockdown! Tag us on Instagram or Twitter @TheDailyLockdown</h5>
                </div>
            </div>
            </div>


 <!--COMMENTS SECTION--> 
        <div class="container">
            <div class="break break-comments row">
            <div class="col-md-7">
                    <p class="title-comments"><svg id="chat-box" class="bi bi-chat-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
                  
                        
                        
                        </svg>COMMENTS</p>
                
                <?php if (count($comments)==0){
                    echo '<p>No comments on this post yet!<p>';
                }
                foreach($comments as $comment) { ?>
 
                <div class="box-comments">
                    <div class="d-flex justify-content-between">
                    <p><span class="button-post"><?php echo $comment->guestEmail; ?></span> SAYS:</p>
                    <a class="btn button" href='?controller=post&action=comDelete&commentID=<?php echo $comment->commentID;?>&postID=<?php echo $post->postID; ?>'> DELETE </a>
                    </div>
                <p class="show-read-more"><?php echo $comment->content; ?></p>
                </div>
                <?php } ?>
            </div>
            </div>
        </div>

    
      <!--FOOTER-->
    <footer>
        <hr>
        <div class="container">
            <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
            <form id="admin-form">
                <p>Admin use only.</p>
                  <a href='#' class="btn button" name="login">LOGIN</a>
                  <p>You are logged in as <?php echo $_SESSION['email'];?></p>
            </form>
            </div>
        </div>
        </div>
    </footer>
       <?php } else{require_once'index.php';}?>
