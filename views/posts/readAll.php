<p></p>
<?php if(!empty($_SESSION['email'])){ ?>
<div class="container">
    <div class="row">
        <div class="col-9">
            <div style="text-align:left;margin:20px 0px;color: #f0451c;" class=" bi bi-caret-right-fill">
                <p><?php
                    if ($count > 3) {
                        echo "Whoops! You have chosen more than three featured posts! Please review below and click update to assign three.";
                    } else if ($count < 3) {
                        echo "Uh oh! You have chosen less than three featured posts! Please review below and click update to assign three";
                    } else {
                        
                    }
                    ?></p></div>
        </div>

<div class="container">
     <div class="f-col-margin text-right" style="margin:20px 0px;">
         <div class="btn-group">
            <a href='?controller=post&action=readAll' class="btn button btn-block" >View All</a>
        </div>
         <div class="btn-group">
            <a href='?controller=post&action=featured'class="btn button btn-block">Featured</a>
        </div>
         <div class="btn-group">
            <a href='?controller=post&action=create' class="btn button create-btn btn-block">Create</a>
        </div>
        </div>
    </div>
</div>

    <table class="table">
        <thead>
            <tr>
                <th class="table-header">Title</th>
                <th class="table-header">Content</th>
                <th class="table-header" width="120px min-width='120px">Date</th>

                <th class="table-header" width="60px">Author</th>
                <th class="table-header" width="60px">Featured</th>
                <th class="table-header" width="120px" min-width='120px' >Actions</th>

            </tr>
        </thead>
        <tbody id="table-body">
<?php foreach ($posts as $post) { ?>
                <tr class="table-row">
                    <td><?php echo $post->title; ?></td>
                    <td><?php $cont = $post->postcontent;
    echo $condense = substr($cont, 0, 100) . '...';
    ?></td>
                    <td><?php echo $post->published; ?></td>

                    <td><?php echo $post->author; ?></td><!-- added for experiment an actual button-->

                    <td><?php $featured = $post->feature;
    if ($featured === 'YES') {
        echo "Yes";
    }
    ?></td>
                    <td>    
                        <a href='?controller=post&action=read&postID=<?php echo $post->postID; ?>'><img src="public/images/read.png" class="list-img mr-1" alt="View Post Button mr-2"></a> 
                        <a href='?controller=post&action=update&postID=<?php echo $post->postID; ?>'><img src="public/images/update.png" class="list-img" alt="Update Post Button"></a>
                        <a href='?controller=post&action=delete&postID=<?php echo $post->postID; ?>'><img src="public/images/delete.png" class="list-img"  alt="Delete Post Button"></a>        

                    </td>
                </tr>
<?php } ?>
  
        </tbody>
    </table>
</div>

<!--FOOTER-->
<footer>
    <hr>
    <div class="container">
        <div class="row footer-margin">
            <div class="col-lg-3 align-self-center">
                <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
            </div>
            <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                <a class="f-menu" href="about.php">About</a>
            </div>
            <div class="col-lg-1 d-none d-lg-block vd">
            </div>
            <div class="f-col-margin col-lg-4 align-self-center">
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                <p class="break-share">Welcome! You are logged in as <?php echo $_SESSION['email'];?></p>
                <a href='?controller=user&action=logout' class="btn button" name="login">LOGOUT</a>
            </div>
        </div>
    </div>
</footer>
<?php } else{require_once'index.php';}?>
<script 

    src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">

</script>
