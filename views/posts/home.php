
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 align-self-center text-center">
            <a href="index.php" class="navbar-brand logo">The Daily <span id="brand">Lockdown</span></a>
            <p>Writing to make your life easier during the lockdown.</p>
            <div id="hero-text">
                <h2 class="text-uppercase"></h2>
                <p>If staying home and staying safe has you going crazy, you've come to the right place! Here you'll find plenty of activities to pass the time and keep yourself grounded!</p>
            </div>
        </div>
        <div class="col-md-6 hero-image">
        </div>
    </div>
</div>

<!--CAROUSEL-->   
<div class="container">
    <h2 id="featured-posts" class="text-uppercase text-center">Featured posts</h2>



    <div class="row">

<?php foreach ($posts as $post) { ?>  
            <div class="col-md-4 d-flex align-items">
                <div class="card mx-auto">
                    <img src="<?php echo $post->image1 ?>" class="img-fluid " alt="<?php echo $post->title ?>">
                        <div class="card-body d-flex flex-column">
                            <h5 class="card-title"><?php echo $post->title; ?></h5>
                            <p class="card-text"><?php
                                $cont = $post->postcontent;
                                echo $condense = substr($cont, 0, 100) . '...';
                                ?></p>
                            <a href='?controller=post&action=comRead&postID=<?php echo $post->postID; ?>' class="text-left mt-auto btn button-hero">READ MORE<svg class="bi bi-caret-right-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 011.659-.753l5.48 4.796a1 1 0 010 1.506z"/></svg></a>
                        </div>
                </div>
            </div>   

<?php } ?>                
    </div>  
</div>

    
      

<div class="container">
   <div class="form-row break-search">
        <div class="form-group col-md-12 text-right">
    
    <!-- Pagination-->
   
<a href="index.php" class="btn button-hero ml-1 mb-1" id="cancelsearch" >Home Page</a>
   

<form name="frmSearch" id="frmSearch" method="post">
     
<!--            <label for="search[keyword]">Search</label>-->
            <input type="text" name="search[keyword]" value id="keyword" maxlength="25">
                <input type="submit" class="btn button-hero" id="submit" value="Search!">
       
       
        </form>
     </div>
     </div>
<!--calling four posts on the page-->
<?php foreach ($pagposts as $pagpost) {   ?>  
    <div class="break row">
                    <div class="col-md-6 order-2 order-md-1 align-self-center">
                        <img class="img-fluid" src="<?php echo $pagpost->image1;  ?>">
                    </div>
                    <div class="col-md-6 order-1 order-md-2 align-self-center">
                            <div class="post-text home-text">
                                <h2 class="text-uppercase"><?php echo $pagpost->title;  ?></h2>
                                <p><?php
    $cont = $pagpost->postcontent;
echo $condense = substr($cont,0,200).'...'; 
?></p>
                                <a href='?controller=post&action=comRead&postID=<?php echo $post = $pagpost->postID;   ?>' class="btn button-post">READ MORE<svg class="bi bi-caret-right-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 011.659-.753l5.48 4.796a1 1 0 010 1.506z"/></svg></a>
                            </div>
                    </div>
           </div>
<?php  }   ?>
    



        <form name="frmSearch" method="post">

                <div class="break-search form-row">
                    <div class="form-group col-md-12 text-center">
                    <input class="button-pag pag-btn" type="submit" name="page" value="1">
                    <input class="button-pag pag-btn" type="submit" name="page" value="2">
                    <input class="pag-btn" type="submit" name="page" value="3">
                    <input class="pag-btn" type="submit" name="page" value="4">
                    <input class="pag-btn" type="submit" name="page" value="5">
                    </div>
                    </div>
            </form>        

        </div>







                        <!--FOOTER-->
                        <footer>
                            <hr>
                                <div class="container">
                                    <div class="row footer-margin">
                                        <div class="col-lg-3 align-self-center">
                                            <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
                                        </div>
                                        <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                                            <a class="f-menu" href="about.php">About</a>
                                        </div>
                                        <div class="col-lg-1 d-none d-lg-block vd">
                                        </div>
                                        <div class="f-col-margin col-lg-4 align-self-center">
                                            <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                                            <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                                            <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                                            <form id="admin-form" controller="user" action="login" method="POST">
                                                <p>Admin use only:</p>
                                                <a href='?controller=user&action=login' class="btn button" name="login">LOGIN</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </footer>
