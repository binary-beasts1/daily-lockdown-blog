<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    
    //error checking pre-uploaded
    $(document).ready(function(){
        $("#Input1").change(function(){
          
          var fileExtension = ['jpeg', 'jpg'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
            $("#submit").prop('disabled', true); 
         //   $("h6").text("Please choose a valid first image before you can submit.");
            $("#imgtype").html("<h6>Please choose a valid first image before you can submit.</h6>");
        } else {
            $("#submit").prop('disabled', false); 
            $("#imgtype").html("<h6></h6>");
        }
        });
    });
    
            $(document).ready(function(){
        $("#Input2").change(function(){
          
          var fileExtension = ['jpeg', 'jpg'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
            $("#submit").prop('disabled', true); 
            $("#imgtype2").html("<h6>Please choose a valid second image before you can submit.</h6>");
        } else {
            $("#submit").prop('disabled', false); 
            $("#imgtype2").html("<h6></h6>");
        }
        });
    });
    
</script>
<?php if(!empty($_SESSION['email'])){ ?>
<div class="container">
    <div class="break row justify-content-center">
        <div class="col-md-8">
            <h2 class="mb-3">CREATE YOUR POST</h2>
            <form name="frmAdd"  action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="title" class="demo-form-field form-control form-control-lg" placeholder="Witty title here!" required  />
                </div>
                <div class="form-group">
                    <textarea name="postcontent" class="demo-form-field form-control form-control-lg" rows="10" placeholder="Input your blog post here!"required ></textarea>
                </div>
                <div class="form-group">
                    <input type="date" name="published" class="demo-form-field form-control form-control-lg" required />
                </div>
                <div class="form-group">
                    <label for='feature'>Featured Post on Homepage?</label> <br>
                        <input type="radio" id="YES" name="feature" required value="YES">
                            <label for="YES">Yes, I'd like this post to be a featured post!</label><br>
                                <input type="radio" id="NO" name="feature" required value="NO">
                                    <label for="NO">No, I would not like this to be a featured post.</label>
                                
                                    </div>
<!--                                    <div class="form-group">
                                        <input type="text" name="author" class="demo-form-field form-control form-control-lg" placeholder="author" required />
                                    </div>
                                        <!--images-->
                                        
                                    <div class="form-group">
                                        <p>First Image</p>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                                        <input id="Input1" type="file" name="uploader" required />
                                    
                                    
                                    
                                    </div>

                                    <div class="form-group">
                                        <p>Second Image</p>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                                        <input id="Input2"  type="file" name="uploader2" required />
                                    </div>
                                    <div class="form-group">
                                        <button id="submit" type="submit" name="add_record" class="btn button-post mt-3"><svg class="bi bi-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                                <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"/>
                                            </svg>Submit</button> 
                                        <!--jquery-->
                                        <div  id="imgtype"></div>
                                        <div  id="imgtype2"></div>
                                    </div>
                                    </form>
                                    </div>
                                    </div>
                                    </div>

                                    <!--FOOTER-->
                                    <footer>
                                        <hr>
                                            <div class="container">
                                                <div class="row footer-margin">
                                                    <div class="col-lg-3 align-self-center">
                                                        <a href="index.php" class="navbar-brand logo"><img src="public/images/Logo.png" width="100%"></a>
                                                    </div>
                                                    <div class="f-col-margin col-lg-4 align-self-center text-lg-center">
                                                        <a class="f-menu" href="about.php">About</a>
                                                    </div>
                                                    <div class="col-lg-1 d-none d-lg-block vd">
                                                    </div>
                                                    <div class="f-col-margin col-lg-4 align-self-center">
                                                        <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/instagram.png" alt="Instagram social icon"></a>
                                                        <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/pinterest.png" alt="Pinterest social icon"></a>
                                                        <a href="#" target="_blank" rel="noopener"><img class="f-social" src="public/images/twitter.png" alt="Twitter social icon"></a>
                                                        <p class="break-share">Welcome! You are logged in as <?php echo $_SESSION['email'];?>.</p>
                                                        <a href="index.php" class="btn button" name="login">LOGOUT</a>
                                                    </div>
                                                </div>
                                            </div>
                                    </footer>
 <?php } else{require_once 'index.php';}?>