<?php
//created user 'lockdown' in SQL with select, insert, update, delete privileges on all tables in the database
// password: lockdown
// used FileZilla server with IP address, now replaced with localhost
class DB {
    
    private static $instance = NULL;
    public static function getInstance() {
      if (!isset(self::$instance)) {
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        self::$instance = new PDO('mysql:host=localhost;dbname=lockdown_blog', 'lockdown', 'lockdown', $pdo_options);
      }
      return self::$instance;
    }
}
