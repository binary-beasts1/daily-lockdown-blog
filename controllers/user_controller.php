<?php
session_start();
class UserController {
    public function login(){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            require_once('views/users/login.php');
        }
        try {
            if(isset($_POST['login'])){  
         
            $found = User::find($_POST['userEmail'], $_POST['password']);
            
            if(($this->userEmail = $_POST['userEmail']) && (password_verify($_POST['password'], $found->password))){
                $_SESSION['email'] = $found->userEmail;
                $_SESSION['author'] = $found->author;
                require_once('views/users/success.php');
            }
             else { 
                echo '<div class="container">
                <div class="break row justify-content-center">
                <div class="col-md-4"><h4> Email or password incorrect. Please try again.</h4> </div></div></div>';
                require_once('views/users/login.php'); 
            
            } 
        }}
        
        catch (Exception $ex){
     return call('pages','error');
}
}


public function logout(){
    $_SESSION = array();
    session_destroy();
    try{
    if(empty($_SESSION)){
        require_once('views/users/logout.php');
    }}
    catch (Exception $ex){
        echo 'unable to clear the session';
    }
}
}
?>


