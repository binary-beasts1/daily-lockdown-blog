<?php
session_start();
class PostController {
    public function featured(){
        $posts = Post::featallcheck();
        $count = Post::count();
        require_once('views/posts/readAll.php');
    }
    
    public function readAll() {
        
        if(!empty($_SESSION)){
          
      // we store all the posts in a variable
        $count = Post::count(); //$posts is used within the view
        $posts = Post::all();
     
      require_once('views/posts/readAll.php');
    }
    else{
            
            $posts = Post::featall();
            $pagposts = Post::paginationselect();
            require_once('views/posts/home.php');
        }
    }
    public function home() {
        if(!empty($_SESSION)){
            $_SESSION = array();
            session_destroy();
        }
  
  $posts = Post::featall();
  $pagposts = Post::paginationselect();
  require_once('views/posts/home.php');  
    }

    
    public function comRead() {
      if (!isset($_GET['postID']))
        return call('pages', 'error');
      try {
      $post = Post::find($_GET['postID']);
      if (!isset($_GET['postID'])) {
          $postID = 1;
        } else{
            $postID = $_GET['postID'];
        }
        
        $next = Post::nextpage($_GET['postID']);
        $previous = Post::previouspage($_GET['postID']);
        
      $comments = Comment::findCommentsPostId($_GET['postID']);
      
       if($_SERVER['REQUEST_METHOD'] == 'GET'){
          require_once('views/posts/comRead.php');
      }
      else { 
            if (isset($_POST['add_comment'])){
                Comment::addComment($_GET['postID']);
                $comments = Comment::findCommentsPostId($_GET['postID']);
            require_once('views/posts/comRead.php');
            }
        
      } }
      
 catch (Exception $ex){
     return call('pages','error');
 }
    }
    
    
   
    public function read() {
      if (!isset($_GET['postID']))
        return call('pages', 'error');
      try{
          if(!empty($_SESSION)){
      // we use the given postID to get the correct post
      $post = Post::find($_GET['postID']);
      $comments = Comment::findCommentsPostId($_GET['postID']);
      require_once('views/posts/read.php');
          }
          else {
              $posts = Post::featall();
            $pagposts = Post::paginationselect();
            require_once('views/posts/home.php');
          }
      }
 catch (Exception $ex){
     return call('pages','error');
 }
    }     
    
    
    public function create() {
      if($_SERVER['REQUEST_METHOD'] == 'GET'){
          require_once('views/posts/create.php');
      }
      else { 
          if(!empty($_SESSION)){
            Post::add();
            $count = Post::count(); //$posts is used within the view
            $posts = Post::all(); //$posts is used within the view
           
          require_once('views/posts/readAll.php');}
          else {
              $posts = Post::featall();
            $pagposts = Post::paginationselect();
            require_once('views/posts/home.php');
          }
      }
      
    }
    
    public function update() {
        
      if($_SERVER['REQUEST_METHOD'] == 'GET'){
          if (!empty($_SESSION)){
          if (!isset($_GET['postID'])){
          return call('pages', 'error');}
        $post = Post::find($_GET['postID']);
        require_once('views/posts/update.php');
          }
          else{
              $posts = Post::featall();
            $pagposts = Post::paginationselect();
            require_once('views/posts/home.php');
          }
        }
      else
          { 
            $postID = $_GET['postID'];
            Post::update($postID);
            $posts = Post::all();
            $count = Post::count(); //$posts is used within the view
            require_once('views/posts/readAll.php');
      }
      
    }
    public function delete() {
            Post::remove($_GET['postID']);
            $count = Post::count(); //$posts is used within the view
            $posts = Post::all();
            require_once('views/posts/readAll.php');
      }
      
      public function comDelete() {
            Comment::remove($_GET['commentID']);
            $post = Post::find($_GET['postID']);
            $comments = Comment::findCommentsPostId($_GET['postID']);
            require_once('views/posts/read.php');
      }
        
      public function userRead() {
      // we expect a url of form ?controller=posts&action=show&postID=x
      // without an postID we just redirect to the error page as we need the post postID to find it in the database
      if (!isset($_GET['postID']))
        return call('pages', 'error');
      try{
      // we use the given postID to get the correct post
      $post = Post::find($_GET['postID']);
      $comments = Comment::findCommentsPostId($_GET['postID']);
      $newcomment = Comment::addComment($_GET['postID']);
      require_once('views/posts/userRead.php');
      
      }
 catch (Exception $ex){
     return call('pages','error');
 }
    }
    }
    
    
  
    
?>
