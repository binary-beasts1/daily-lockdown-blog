<?php

class PagesController {
   
    public function home() {
        session_start();
        if(!empty($_SESSION)){
            $_SESSION = array();
            session_destroy();
      require_once('views/posts/home.php');
    }
    if(empty($_SESSION)){
        require_once('views/posts/home.php');
    }
        else {
            require_once('views/pages/error.php');
        }
    }

    public function error() {
      require_once('views/pages/error.php');
    }
    
    public function about() {
        session_start();
            $_SESSION = array();
            session_destroy();
        if(empty($_SESSION)){
            require_once('views/pages/about.php');
        }
        else{
            require_once('views/pages/error.php');
        }
    }
    
}
